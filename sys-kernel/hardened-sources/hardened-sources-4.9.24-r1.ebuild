# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="7"
ETYPE="sources"
K_WANT_GENPATCHES="base"
K_GENPATCHES_VER="24"
K_DEBLOB_AVAILABLE="1"

inherit kernel-2
detect_version
detect_arch

GBPF="genpatches-${KV_MAJOR}.${KV_MINOR}-${K_GENPATCHES_VER}.base.tar.xz"
HEPF="hardened-patches-${KV_MAJOR}.${KV_MINOR}.${KV_PATCH}-1.extras.tar.bz2"
UNIPATCH_LIST="${DISTDIR}/${HEPF}"
UNIPATCH_EXCLUDE="
	1500_XATTR_USER_PREFIX.patch
	2900_dev-root-proc-mount-fix.patch"

DESCRIPTION="Hardened kernel sources (kernel series ${KV_MAJOR}.${KV_MINOR})"
HOMEPAGE="https://www.gentoo.org/proj/en/hardened/"
SRC_URI="
	${KERNEL_URI} ${ARCH_URI}
	https://dev.gentoo.org/~mpagano/genpatches/tarballs/${GBPF} https://grsecurity.gitlab.io/gentoo/distfiles/${GBPF} ${GENPATCHES_URI}
	https://dev.gentoo.org/~blueness/hardened-sources/hardened-patches/${HEPF} https://grsecurity.gitlab.io/gentoo/distfiles/${HEPF}"

KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~sparc ~x86"
RESTRICT="primaryuri"
IUSE="deblob"

RDEPEND=">=sys-devel/gcc-4.5"

pkg_postinst() {
	kernel-2_pkg_postinst

	local GRADM_COMPAT="sys-apps/gradm-3.1*"

	einfo "For more info on this patchset, see ${HOMEPAGE}"

	ewarn
	ewarn "Users of grsecurity's RBAC system must ensure they are using"
	ewarn "${GRADM_COMPAT}, which is compatible with ${PF}."
	ewarn "It is strongly recommended that the following command is issued"
	ewarn "prior to booting a ${PF} kernel for the first time:"
	ewarn
	ewarn "emerge -na =${GRADM_COMPAT}"
	ewarn
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
